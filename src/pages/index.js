import React from 'react'
import ReactMarkdown from 'react-markdown'
import Link from 'gatsby-link'
import Header from '../components/header'


const IndexPage = ({data}) => (
  <div>
    <div className="background">
      <Header siteTitle={data.site.siteMetadata.title} navLinks={data.allNavLink.edges[0].node.pages} />
      <div className="container">
        <div className="hero">
          <h1>{data.allPage.edges[0].node.title}</h1>
          <ReactMarkdown source={data.allPage.edges[0].node.body} />
          {console.log(data)}
        </div>
      </div>  
    </div>
    <div className="container under-background">
      <div className="columns">
        <div className="column col-10 col-xs-12 col-mx-auto">
            <h5>My Latest Thoughts</h5>
            <div className="columns">
            <div className="column col-4 col-sm-12 mb-2">
              <div className="card">
                <div className="card-image">
                  <img className="img-responsive" src="https://via.placeholder.com/350x250"/>
                </div>
                <div className="card-header">
                  <div className="card-title h5">First title goes here</div>
                </div>
                <div className="card-body">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vitae luctus ante. Sed at leo mollis, varius lacus vel, finibus felis.
                </div>
                <div className="card-footer">
                  
                </div>
              </div>
            </div>

            <div className="column col-4 col-sm-12 mb-2">
              <div className="card">
                <div className="card-image">
                  <img className="img-responsive" src="https://via.placeholder.com/350x250"/>
                </div>
                <div className="card-header">
                  <div className="card-title h5">Second title goes here</div>
                </div>
                <div className="card-body">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vitae luctus ante. lacus vel, finibus felis.
                </div>
                <div className="card-footer">
                  
                </div>
              </div>
            </div>

            <div className="column col-4 col-sm-12 mb-2">
              <div className="card ">
                <div className="card-image">
                  <img className="img-responsive" src="https://via.placeholder.com/350x250" />
                </div>
                <div className="card-header">
                  <div className="card-title h5">Third title goes here</div>
                </div>
                <div className="card-body">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vitae luctus ante. Sed at leo mollis, varius lacus vel, finibus felis.
                </div>
                <div className="card-footer">
                 
                </div>
              </div>
            </div>
          </div>


          </div>
        </div>
      </div>
    </div>
)

export default IndexPage

export const data = graphql`
query HomePageQuery {
  site {
      siteMetadata {
        title
      }
    }
  allNavLink {
      edges {
        node {
          pages {
            slug
          }
        }
      }
    }
  allPage ( filter: { id : { eq: "cjkxbwcm1b4b40989jheqoa6e" } } ) {
      edges {
        node {
          id
          title
          subtitle
          usesHero
          body
        }
      }
    }
}
`