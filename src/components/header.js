import React from 'react'
import Link from 'gatsby-link'
import Logo from '../images/logo3.png'

const Header = ({ navLinks }) => (
  <header className="navbar">
    <section className="navbar-section">
      <span className="brand">TYLER CHANCE</span>
    </section>
    <section className="navbar-section hide-xs">
          {navLinks.map( link => {
            return <a href="..." className="btn btn-link" key={link.slug}>{link.slug}</a>
          } )}
    </section>
  </header>
)

export default Header

