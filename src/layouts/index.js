import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import '../styles/index.scss'

const Layout = ({ children, data }) => (
  <div className="content">
  <div className="inside-content">
  <Helmet
        title={data.site.siteMetadata.title}
        meta={[
          { name: 'description', content: 'Sample' },
          { name: 'keywords', content: 'sample, something' },
        ]}
      />
        
          {children()}
  </div>
  </div>
)

Layout.propTypes = {
  children: PropTypes.func,
}

export default Layout

export const query = graphql`
  query poopers {
    site {
      siteMetadata {
        title
      }
    }
  }
`
