require('dotenv').config()

module.exports = {
  siteMetadata: {
    title: 'Tyler Chance',
  },
  plugins: [
    {
      resolve: 'gatsby-source-graphcms',
      options: {
        endpoint: process.env.GRAPHCMS_ENDPOINT,
        token: process.env.GRAPHCMS_TOKEN,
        query: `{
            navLinks {
              id
              pages {
                id
                slug
              }
            }
            pages {
              id
              slug
              title
              subtitle
              usesHero
              body
            }
        }`
    }
  },
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-sass',
    'gatsby-plugin-netlify'
  ],
}
